<!DOCTYPE html>
<html>
	<?php include_once('head.php'); ?>
	
	<body>
		<?php include_once('header.php'); ?>

        <p id='welcome'>
            <?php echo ($lang['WELCOME']); ?>
        </p>

		<?php include_once('footer.php'); ?>
	</body>
</html>