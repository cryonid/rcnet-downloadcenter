<!DOCTYPE html>
<html>
	<?php include_once('../head.php'); ?>
	
	<body>
		<?php include_once('../header.php'); ?>
		
		<div id='winxp'>
			<?php include_once('design.php'); ?>
			<div id='content'>
                <div id='index'>
			    	<p><a href=<?php echo ($configuration['site_dir'] . 'winnt/images'); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/winxp/winnt_images.gif'); ?> /><br /><?php echo ($lang['IMAGES']); ?></a></p>
                    <p><a href=<?php echo ($configuration['site_dir'] . 'winnt/updates'); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/win9x/wininstall.gif'); ?> /><br /><?php echo ($lang['GAMES']); ?></a></p>
		    		<p><a href=<?php echo ($configuration['site_dir'] . 'winnt/drivers'); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/winxp/winnt_drivers.gif'); ?> /><br /><?php echo ($lang['DRIVERS']); ?></a></p>
	    			<p><a href=<?php echo ($configuration['site_dir'] . 'winnt/softwares'); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/winxp/winnt_softwares.gif'); ?> /><br /><?php echo ($lang['SOFTWARES']); ?></a></p>
                    <p><a href=<?php echo ($configuration['site_dir'] . 'winnt/games'); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/winxp/winnt_games.gif'); ?> /><br /><?php echo ($lang['GAMES']); ?></a></p>
                </div>
			</div>
		</div>
		
		<?php include_once('../footer.php'); ?>
	</body>
</html>