-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  Dim 06 août 2017 à 16:16
-- Version du serveur :  10.1.25-MariaDB
-- Version de PHP :  7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `downloadcenter`
--

-- --------------------------------------------------------

--
-- Structure de la table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `last_connection` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `configuration`
--

CREATE TABLE `configuration` (
  `site_name` varchar(255) NOT NULL,
  `site_dir` text NOT NULL,
  `site_home` text NOT NULL,
  `version` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains download center''s configuration';

--
-- Déchargement des données de la table `configuration`
--

INSERT INTO `configuration` (`site_name`, `site_dir`, `site_home`, `version`) VALUES
('', '', '', '2.0');

-- --------------------------------------------------------

--
-- Structure de la table `files`
--

CREATE TABLE `files` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `system` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `download_url` text NOT NULL,
  `nb_downloads` int(11) NOT NULL,
  `file_format` varchar(255) NOT NULL,
  `compression` tinyint(1) NOT NULL,
  `quality` int(11) NOT NULL,
  `version` varchar(255) NOT NULL,
  `nickname` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `language` varchar(255) NOT NULL,
  `release_date` date NOT NULL,
  `upload_date` date NOT NULL,
  `description` text NOT NULL,
  `minimal_sys_req` text NOT NULL,
  `recommended_sys_req` text NOT NULL,
  `file_size` float NOT NULL,
  `size_unit` varchar(255) NOT NULL,
  `nb_supports` int(11) NOT NULL,
  `support_type` varchar(255) NOT NULL,
  `sha1` text NOT NULL,
  `md5` text NOT NULL,
  `icon32_url` text NOT NULL,
  `icon16_url` text NOT NULL,
  `cover_url` text NOT NULL,
  `screenshot_url` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains download center''s file infos';

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
