<header>
	<a href=<?php echo ($configuration['site_home']); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/header/arrow.gif'); ?> /></a>

	<span id='site_title'><?php echo ($lang['SITE_TITLE'] . ' - ' . $configuration['site_name']); ?></span>

    <span id='left'><?php echo ($lang['SELECT_LANG'] . ' '); ?> 
        <?php
        $language_dir = array_diff(scandir($_SERVER['DOCUMENT_ROOT'] . $configuration['site_dir'] . 'languages', 1), array('.', '..'));

        for ($i = 0; $i < count($language_dir); $i++)
        {
            echo ('<a href=' . $configuration['site_dir'] . '?lang=' . str_replace('.php', '', $language_dir[$i]) . '>[' . strtoupper(str_replace('.php', '', $language_dir[$i])) . ']</a>');
        }
        
        echo (' || ' . date($lang['DATE_FORMAT'] . ' - ' . $lang['TIME_FORMAT']));
        ?>
    </span>
</header>