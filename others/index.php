<!DOCTYPE html>
<html>
	<?php include_once('../head.php'); ?>
	
	<body>
		<?php include_once('../header.php'); ?>
		
		<div id='tos'>
			<?php include_once('design.php'); ?>
			<div id='content'>
                <div id='index'>
			    	<p><a href=<?php echo ($configuration['site_dir'] . 'others/images'); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/win9x/win9x_images.gif'); ?> /><br /><?php echo ($lang['IMAGES']); ?></a></p>
                    <p><a href=<?php echo ($configuration['site_dir'] . 'others/updates'); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/win9x/wininstall.gif'); ?> /><br /><?php echo ($lang['GAMES']); ?></a></p>
		    		<p><a href=<?php echo ($configuration['site_dir'] . 'others/drivers'); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/win9x/win9x_drivers.gif'); ?> /><br /><?php echo ($lang['DRIVERS']); ?></a></p>
	    			<p><a href=<?php echo ($configuration['site_dir'] . 'others/softwares'); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/win9x/win9x_softwares.gif'); ?> /><br /><?php echo ($lang['SOFTWARES']); ?></a></p>
                    <p><a href=<?php echo ($configuration['site_dir'] . 'others/games'); ?>><img src=<?php echo ($configuration['site_dir'] . 'images/win9x/win9x_games.gif'); ?> width="32" height="32" /><br /><?php echo ($lang['GAMES']); ?></a></p>
                </div>
			</div>
		</div>
		
		<?php include_once('../footer.php'); ?>
	</body>
</html>