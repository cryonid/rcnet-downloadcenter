<?php
include_once('head.php');

if ($_GET['id'])
{
	$query = $sql->query('SELECT download_url FROM files WHERE id=' . $_GET['id']);
	$data = $query->fetch();

	if ($data)
	{
		if ($data['download_url'])
		{
			$sql->exec('UPDATE files SET nb_downloads = nb_downloads+1 WHERE id = ' . $_GET['id']);
            
			header('Location: ' . $data['download_url']);
			exit();
		}
		else
		{
			header('Location: ' . $configuration['site_dir'] . '404.php');
			exit();
		}
	}
	else
	{
		header('Location: ' . $configuration['site_dir'] . '404.php');
		exit();
	}

	$query->closeCursor();
}
else
{
	header('Location: ' . $configuration['site_dir']);
	exit();
}
?>