<?php
if (isSet($_GET['id']))
{
	$query = $sql->query('SELECT * FROM files WHERE id=' . $_GET['id']);
	$data = $query->fetch();

	if ($data)
	{
        ?>

        <h2><?php echo ($data['name']); ?></h2>

        <h3><?php echo ($lang['EDITOR'] . ' ' . $data['company']); ?></h3>

        <?php
        if ($data['icon32_url'])
        {
            echo ('<img src=' . $data['icon32_url'] . ' />');
        }
        ?>

        <?php
        switch ($data['quality'])
        {
            case 0:
                $quality['icon'] = 'images/icons/lvl0flag.gif';
                $quality['color'] = 'gray';
                $quality['text'] = strtolower($lang['LVL0']);
            break;
            
            case 1:
                $quality['icon'] = 'images/icons/lvl1flag.gif';
                $quality['color'] = 'red';
                $quality['text'] = strtolower($lang['LVL1']);
            break;
            
            case 2:
                $quality['icon'] = 'images/icons/lvl2flag.gif';
                $quality['color'] = 'orange';
                $quality['text'] = strtolower($lang['LVL2']);
            break;
            
            case 3:
                $quality['icon'] = 'images/icons/lvl3flag.gif';
                $quality['color'] = 'blue';
                $quality['text'] = strtolower($lang['LVL3']);
            break;
            
            case 4:
                $quality['icon'] = 'images/icons/lvl4flag.gif';
                $quality['color'] = 'green';
                $quality['text'] = strtolower($lang['LVL4']);
            break;
            
            case 5:
                $quality['icon'] = 'images/icons/sealofquality.gif';
                $quality['color'] = 'green';
                $quality['text'] = strtolower($lang['LVL5']);
            break;
            
            default:
                $quality['icon'] = '';
                $quality['color'] = '';
                $quality['text'] = strtolower($lang['UNKNOWN_LVL']);
            break;
        }
        ?>

        <h3><a href=<?php echo ($configuration['site_dir'] . 'download.php?id='. $_GET['id']); ?>><strong><?php echo ($lang['DOWNLOAD']); ?></strong></a> <?php echo ('<img src=' . $configuration['site_dir'] . $quality['icon'] . ' /> <em style="font-size: 12px"><a href="http://retrocompute.net/viewtopic.php?f=5&t=18" style="color: ' . $quality['color'] . '; text-decoration: none">(' . $quality['text'] . ')</a></em>'); ?>

        <span style="font-size: 9px"><?php echo ($lang['DOWNLOADED'] . ' ' . $data['nb_downloads'] . ' ' . $lang['TIMES']); ?></span></h3>

        <?php
        if ($data['cover_url'])
        {
            echo ('<a href=' . $data['cover_url'] . '><img width="320px" src=' . $data['cover_url'] . ' /></a>');
        }
        else
        {
            echo ('<em>' . $lang['NO_COVER'] . '</em>');
        }
        ?>

        <p><strong><?php echo ($lang['FILE_FORMAT']); ?></strong><br />
        <?php
        switch ($data['file_format'])
        {
            case 'bin':
                $file_format = $lang['FF_BIN'];
            break;
            case 'iso':
                $file_format = $lang['FF_ISO'];
            break;
            case 'mds':
                $file_format = $lang['FF_MDS'];
            break;
            case 'ima':
                $file_format = $lang['FF_IMA'];
            break;
            case 'img':
                $file_format = $lang['FF_IMG'];
            break;
            case 'other':
                $file_format = $lang['FF_OTHER'];
            break;
            default:
                $file_format = strtoupper($data['file_format']);
            break;
        }

        echo ($file_format);

        if ($data['compression'])
        {
            echo (' (' . $lang['COMPRESSION'] . ')');
        }
        ?>
        </p>

        <?php
        if ($data['version'])
        {
            echo ('<p><strong>' . $lang['VERSION'] . '</strong><br />' . $data['version'] . '</p>');
        }
        ?>

        <p><strong><?php echo ($lang['SHARED_BY']); ?></strong><br />
        <?php echo $data['nickname']; ?>
        </p>

        <p><strong><?php echo ($lang['FILE_SOURCE']); ?></strong><br />
        <?php
        if ($data['source'])
        {
            echo ($data['source']);
        }
        else
        {
            echo ($lang['UNKNOWN']);
        }
        ?>
        </p>

        <p><strong><?php echo ($lang['LANGUAGE']); ?></strong><br />
        <?php echo ($data['language']); ?>
        </p>

        <?php
        if (date($data['release_date']) != '0000-00-00')
        {
            echo ('<p><strong>' . $lang['RELEASE_DATE'] . '</strong><br />' . date($lang['DATE_FORMAT'], strtotime($data['release_date'])) . '</p>');
        }
        ?>

        <p><strong><?php echo ($lang['UPLOAD_DATE']); ?></strong><br />
        <?php echo (date($lang['DATE_FORMAT'], strtotime($data['upload_date']))); ?>
        </p>

        <p><strong><?php echo ($lang['DESCRIPTION']); ?></strong><br />
        <?php echo ($data['description']); ?>
        </p>

        <p><strong><?php echo ($lang['MINIMAL_SYS_REQ']); ?></strong><br />
        <?php echo ($data['minimal_sys_req']); ?>
        </p>

        <?php
        if ($data['recommended_sys_req'])
        {
            echo ('<p><strong>' . $lang['RECOMMENDED_SYS_REQ'] . '</strong><br />' . $data['recommended_sys_req'] . '</p>');
        }
        ?>

        <p><strong><?php echo ($lang['FILE_SIZE']); ?></strong><br />
        <?php
            if ($data['file_size'])
            {
                switch ($data['size_unit'])
                {
                    case 'byte':
                        switch ($data['size_unit'])
                        {
                            case ($data['size_unit'] <= 1):
                                $file_unit = $lang['BYTE'];
                            break;

                            case ($data['size_unit'] > 1):
                                $file_unit = $lang['BYTES'];
                            break;
                        }
                    break;

                    case 'kbyte':
                        $file_unit = $lang['KB'];
                    break;

                    case 'mbyte':
                        $file_unit = $lang['MB'];
                    break;

                    case 'gbyte':
                        $file_unit = $lang['MB'];
                    break;
                }

                echo ($data['file_size'] . ' ' . $file_unit);
            }
            else
            {
                echo ($lang['UNKNOWN']);
            }
        ?>
        </p>

        <p><strong><?php echo ($lang['SUPPORTS']); ?></strong><br />
        <?php
            if ($data['nb_supports'])
            {
                switch ($data['support_type'])
                {
                    case 'cd':
                        $support_type = $lang['CD'];
                    break;

                    case 'dvd':
                        $support_type = $lang['DVD'];
                    break;

                    case 'floppydisk':
                        switch ($data['nb_supports'])
                        {
                            case ($data['nb_supports'] <= 1):
                                $support_type = $lang['FLOPPY_DISK'];
                            break;

                            case ($data['nb_supports'] > 1):
                                $support_type = $lang['FLOPPY_DISKS'];
                            break;
                        }
                    break;
                }

                echo ($data['nb_supports'] . ' ' . $support_type);
            }
            else
            {
                echo ($lang['UNKNOWN']);
            }
        ?>
        </p>

        <p><strong><?php echo ($lang['SHA1']); ?></strong><br />
        <?php echo ($data['sha1']); ?>
        </p>

        <p><strong><?php echo ($lang['MD5']); ?></strong><br />
        <?php echo ($data['md5']); ?>
        </p>

        <?php
        if ($data['screenshot_url'])
        {
        ?>
            <p><strong><?php echo ($lang['SCREENSHOT']); ?></strong><br />
            <a href=<?php echo ($data['screenshot_url']); ?>><img width='320px' src='<?php echo ($data['screenshot_url']); ?>' /></a>
            </p>
        <?php
        }
        else
        {
            echo ('<em>' . $lang['NO_SCREENSHOT'] . '</em>');
        }
        ?>

        <?php
	}
	else
	{
		echo ('<p>' . $lang['FILE_NOT_FOUND'] . '<br />' . $lang['CHECK_ID_OR_CONTACT'] . '</p>');
	}
    
	$query->closeCursor();
}
?>