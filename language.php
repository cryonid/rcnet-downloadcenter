<?php
session_start();

if (isSet($_GET['lang']))
{
        $lang = $_GET['lang'];
        $_SESSION['dlcenter_lang'] = $lang;
        setcookie('dlcenter_lang', $lang, time() + (3600 * 24 * 30), null, null, false, true);
}
else if (isSet($_SESSION['dlcenter_lang']))
{
        $lang = $_SESSION['dlcenter_lang'];
}
else if (isSet($_COOKIE['dlcenter_lang']))
{
        $lang = $_COOKIE['dlcenter_lang'];
}
else
{
        $lang = 'fr';
}

if (file_exists($_SERVER['DOCUMENT_ROOT'] . $configuration['site_dir'] . 'languages/' . $lang . '.php'))
{
        include_once($_SERVER['DOCUMENT_ROOT'] . $configuration['site_dir'] . 'languages/' . $lang . '.php');
}
else
{
        include_once($_SERVER['DOCUMENT_ROOT'] . $configuration['site_dir'] . 'languages/fr.php');
}
?>