<!DOCTYPE html>
<html>
	<?php include_once('../../head.php'); ?>
	
	<body>
		<?php include_once('../../header.php'); ?>
		
		<div id='win9x'>
            <?php include_once('../design.php'); ?>
            <div id='content'>
                <div id='left_content'>
                    <?php include_once('../../left_content.php'); ?>
                    <span id='separator'></span>
                </div>
                <div id='right_bar'>
                    <?php include_once('../../right_bar.php'); ?>
                </div>
            </div>
		</div>
		
		<?php include_once('../../footer.php'); ?>
	</body>
</html>