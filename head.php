<head>
	<?php
	//Please complete the $database variable to connect the website to your database by replacing capital words with asked values.
	$database = array(
	    'host' => 'localhost',
	    'dbname' => 'downloadcenter',
	    'user' => 'root',
		'password' => ''
	);

	//DO NOT TOUCH THE FOLLOWING SETTINGS UNLESS YOU KNOW WHAT ARE YOU DOING!
	try
	{
        $sql = new PDO('mysql:host=' . $database['host'] . ';dbname=' . $database['dbname'] . ';charset=utf8', $database['user'], $database['password']);
	}
	catch (Exception $e)
	{
        die('Error: ' . $e->getMessage());
	}

	$query = $sql->query('SELECT * FROM configuration');
	$configuration = $query->fetch();

    include_once($_SERVER['DOCUMENT_ROOT'] . $configuration['site_dir'] . 'language.php');

    $query->closeCursor();
	?>

	<title><?php echo ($lang['SITE_TITLE'] . ' - ' . $configuration['site_name']); ?></title>

	<meta charset='utf-8' />

	<link rel='stylesheet' href="<?php echo ($configuration['site_dir'] . 'style.css'); ?>" />
</head>
