<?php
/*
Fichier de traduction du centre de téléchargement en français
Auteur : Cryonid

RÈGLES À RESPECTER PENDANT VOS TRADUCTIONS :
- Respectez la casse. Les lettres en majuscules doivent rester en majuscules
- Remplacez UNIQUEMENT les textes donc :
    - Ne supprimez pas les balises (<br/>, <p></p>, etc...) s'il y a
    - Ne supprimez pas les variables ($configuration, etc...) s'il y a
*/

$lang = array();

//Global
$lang['SITE_TITLE'] = 'Centre de téléchargement';
$lang['WELCOME'] = 'Bienvenue sur le centre de téléchargement de ' . $configuration['site_name'] . ' !<br /><br />Pour commencer, cliquez sur le système d\'exploitation de votre choix ci dessous. Chaque système d\'exploitation correspond à une catégorie qui contient à la fois des images d\'installation du système en question, des logiciels, des jeux et des pilotes prévus pour s\'installer sur le système en question.<br /><br />Si vous rencontrez le moindre problème, merci de vous adresser à un administrateur sur le forum.';
$lang['VERSION'] = 'Version :';
$lang['404'] = '<h1>Erreur 404</h1><p>Désolé, la page que vous avez demandé n\'existe pas.<br /><br />Cliquez sur <a href=' . $configuration['site_dir'] . '>CTRL+ALT+SUPPR</a> pour retourner à l\'accueil</p>';
$lang['SELECT_LANG'] = 'Sélectionner une langue :';


//Footer - System names
$lang['MSDOS'] = 'DOS/3.x';
$lang['WIN9X'] = 'Win. 9x';
$lang['WINNT'] = 'Win. NT';
$lang['MACINTOSH'] = 'Macintosh';
$lang['LINUX'] = 'Linux';
$lang['OTHERS'] = 'Autres';

//Footer - Watermark
$lang['NBFILES'] = 'fichiers en ligne';
$lang['NBDOWNLOADS'] = 'téléchargements effectués';
$lang['WATERMARK'] = '<br />Designé et développé par <a href="http://retrocompute.net/memberlist.php?mode=viewprofile&u=2">Cryonid</a> et <a href="http://retrocompute.net/memberlist.php?mode=viewprofile&u=48">LiveKiller44</a>, icônes et boutons par <a href="https://retrocompute.net/memberlist.php?mode=viewprofile&u=50">Deksor</a><br />Code disponible sur <a href="https://github.com/Cryonid/rcnet-downloadcenter">GitHub</a> sous <a href="https://github.com/Cryonid/rcnet-downloadcenter/blob/master/LICENSE">licence libre GNU GPLv3</a>';



//Categories
$lang['IMAGES'] = 'Images';
$lang['UPDATES'] = 'Mises à jour';
$lang['DRIVERS'] = 'Pilotes';
$lang['SOFTWARES'] = 'Logiciels';
$lang['GAMES'] = 'Jeux';



//Windows
$lang['BACK'] = 'Retour';
$lang['WINNT_TITLE'] = 'Windows NT/2000/XP';
$lang['WIN9X_TITLE'] = 'Windows 95/98/ME';
$lang['MSDOS_TITLE'] = 'MS-DOS/Windows 3.x';



//Right bar
$lang['EDITOR'] = 'Éditeur :';
$lang['DOWNLOAD'] = 'Télécharger';
$lang['LVL0'] = 'Niveau 0 ou inconnu';
$lang['LVL1'] = 'Niveau 1';
$lang['LVL2'] = 'Niveau 2';
$lang['LVL3'] = 'Niveau 3';
$lang['LVL4'] = 'Niveau 4';
$lang['LVL5'] = 'Niveau 5';
$lang['UNKNOWN_LVL'] = 'qualité de la copie inconnue';
$lang['DOWNLOADED'] = 'téléchargé';
$lang['TIMES'] = 'fois';
$lang['NO_COVER'] = 'Aucune jaquette n\'a été trouvée. Envoyez votre jaquette à un administrateur via le formulaire de contact !';
$lang['FILE_FORMAT'] = 'Format du fichier :';
$lang['COMPRESSION'] = 'avec compression';
$lang['SHARED_BY'] = 'Partagé par :';
$lang['FILE_SOURCE'] = 'Source du fichier';
$lang['LANGUAGE'] = 'Langue :';
$lang['RELEASE_DATE'] = 'Date de publication de l\'oeuvre :';
$lang['DATE_FORMAT'] = 'd/m/Y';
$lang['TIME_FORMAT'] = 'H:i';
$lang['UPLOAD_DATE'] = 'Date de mise en ligne sur le centre :';
$lang['DESCRIPTION'] = 'Description :';
$lang['MINIMAL_SYS_REQ'] = 'Configuration minimale requise :';
$lang['RECOMMENDED_SYS_REQ'] = 'Configuration recommandée :';
$lang['FILE_SIZE'] = 'Taille du fichier :';
$lang['UNKNOWN'] = 'Inconnu';
$lang['SUPPORTS'] = 'Support(s) :';
$lang['SHA1'] = 'Somme SHA1 :';
$lang['MD5'] = 'Somme MD5 :';
$lang['SCREENSHOT'] = 'Capture d\'écran :';
$lang['NO_SCREENSHOT'] = 'Aucune capture d\'écran n\'a été mise en ligne. Envoyez vos captures d\'écran à un administrateur !';
$lang['FILE_NOT_FOUND'] = 'Le fichier demandé est introuvable.';
$lang['CHECK_ID_OR_CONTACT'] = 'Vérifiez que l\'ID ou que l\'URL du fichier est correct, sinon merci de contacter un administrateur (ou sélectionnez un fichier dans la liste).';



//Admin panel
$lang['TXT_FILE_GEN'] = 'Générateur de texte';
$lang['COPY_PASTE'] = 'Copiez-collez ce texte dans un fichier .txt à fournir avec votre téléchargement :';
$lang['USERNAME'] = 'Nom d\'utilisateur :';
$lang['PASSWORD'] = 'Mot de passe :';
$lang['ERROR_NO_LOGIN'] = 'Erreur : pas de nom d\'utilisateur ou de mot de passe.';
$lang['INCORRECT_PASSWORD'] = 'Mot de passe incorrect';
$lang['LOGIN_REDIR'] = 'Redirection vers la page de connexion...';
$lang['BACK_TO_SITE'] = 'Retour au centre';
$lang['BACK_TO_ORIGINAL_SITE'] = 'Retour au forum';
$lang['ADMIN_HOME'] = 'Accueil du panel';
$lang['ADMIN_ADD'] = 'Ajouter un fichier';
$lang['ADMIN_MODIFY'] = 'Modifier un fichier';
$lang['ADD_TITLE'] = 'Ajouter un fichier à la collection de';
$lang['CONTENT_NAME'] = 'Nom du contenu :';
$lang['COMPANY'] = 'Éditeur :';
$lang['CATEGORY'] = 'Catégorie :';
$lang['OS'] = 'Système d\'exploitation :';
$lang['WARNING_OS'] = 'ATTENTION : si le contenu n\'est pas compatible avec tous les systèmes d\'exploitation d\'une même catégorie,<br />merci de le préciser dans les parties "Configuration minimale requise" et "Configuration recommandée"';
$lang['FILE_URL'] = 'URL du fichier :';
$lang['FILE_SOURCE'] = 'Source du/des fichier :';
$lang['FILE_FORMAT'] = 'Format du/des fichier :';
$lang['FF_BIN'] = 'BIN/CUE';
$lang['FF_ISO'] = 'ISO';
$lang['FF_IMA'] = 'IMA';
$lang['FF_MDS'] = 'MDS';
$lang['FF_IMG'] = 'IMG';
$lang['FF_OTHER'] = 'Autre (txt, pdf, etc...)';
$lang['OTHER'] = 'Autre';
$lang['NO_COMPRESSION'] = 'sans compression';
$lang['DUMP_QUALITY'] = 'Qualité du dump :';
$lang['LVL'] = 'Niveau';
$lang['SHARED_BY'] = 'Partagé par (pseudo/nom) :';
$lang['INFO_DATE_FORMAT'] = '(si écrit à la main : format AAAA-MM-JJ, exemple : 8 janvier 1994 se traduira par "1994-01-08")';
$lang['WARNING_LINEBREAK'] = 'Merci de ne pas faire de retours à la ligne';
$lang['USE_DOT'] = 'ATTENTION : Utilisez impérativement le <strong>point</strong> si vous souhaitez entrer un nombre décimal';
$lang['BYTE'] = 'octet';
$lang['BYTE_S'] = 'octet(s)';
$lang['BYTES'] = 'octets';
$lang['KB'] = 'Ko';
$lang['MB'] = 'Mo';
$lang['GB'] = 'Go';
$lang['NB_SUPPORTS'] = 'Nombre de supports (CD/DVD/disquettes) à utiliser :';
$lang['CD'] = 'CD';
$lang['DVD'] = 'DVD';
$lang['FLOPPY_DISK'] = 'disquette';
$lang['FLOPPY_DISKS'] = 'disquettes';
$lang['FLOPPY_DISK_S'] = 'disquette(s)';
$lang['ICON32_URL'] = 'URL de l\'icône (32 x 32 px) :';
$lang['ICON16_URL'] = 'URL de l\'icône (16 x 16 px) :';
$lang['LIST_LOCAL_ICONS'] = 'Retrouvez la liste des icônes disponibles localement en cliquant ici';
$lang['TYPE'] = 'Tapez';
$lang['FILE_NAME'] = '[nom du fichier]';
$lang['USE_LOCAL_ICON'] = 'pour utiliser une icône locale';
$lang['COVER_URL'] = 'URL de la jaquette :';
$lang['SCREENSHOT_URL'] = 'URL d\'un screenshot :';
$lang['ERROR_SEND'] = 'Erreur lors du traitement';
$lang['ERROR_ADD'] = 'Il manque des informations, merci de compléter le formulaire jusqu\'au bout';
$lang['SUCCESS_ADD'] = 'Fichier ajouté avec succès';
$lang['SUCCESS_MODIFY'] = 'Fichier modifié avec succès';
$lang['SEND_REDIR'] = 'Redirection dans 3 secondes vers la page d\'ajout...';
$lang['SELECT_CATEGORY'] = 'Séléctionnez la catégorie et le système d\'exploitation du fichier :';
$lang['SELECT_FILE'] = 'Sélectionnez le fichier dans la liste :';
$lang['MODIFY_TITLE'] = 'Modifier les informations d\'un fichier de la collection de';
$lang['KEEP_DEFINED_VALUE'] = 'Garder la valeur définie';
$lang['UNKNOWN_COMPRESSION'] = 'inconnu';
?>