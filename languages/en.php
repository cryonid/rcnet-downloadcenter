<?php
/*
Download center english translation file
Author : Cryonid

RULES TO FOLLOW WHILE TRANSLATING:
- Respect uppercase and lowercase letters.
- ONLY replace texts so:
    - Do not touch or delete HTML tags (<br/>, <p></p>, etc...) if present
    - Do not touch or delete variables ($configuration, etc...) if present
*/

$lang = array();

//Global
$lang['SITE_TITLE'] = 'Download center';
$lang['WELCOME'] = 'Welcome to ' . $configuration['site_name'] . '\'s download center!<br /><br />Please click on the operating system of your choice. Each operating system is a category which contains operating system\'s installation images, softwares, games and drivers.<br /><br />If you encounter any problem, please report it to an administrator on the forum.';
$lang['VERSION'] = 'Version :';
$lang['404'] = '<h1>404 Error</h1><p>Sorry, the webpage you asked doesn\'t exist.<br /><br />Click on <a href=' . $configuration['site_dir'] . '>CTRL+ALT+SUPPR</a> to go back to homepage</p>';
$lang['SELECT_LANG'] = 'Select a language :';


//Footer - System names
$lang['MSDOS'] = 'DOS/3.x';
$lang['WIN9X'] = 'Win. 9x';
$lang['WINNT'] = 'Win. NT';
$lang['MACINTOSH'] = 'Macintosh';
$lang['LINUX'] = 'Linux';
$lang['OTHERS'] = 'Others';

//Footer - Watermark
$lang['NBFILES'] = 'files uploaded';
$lang['NBDOWNLOADS'] = 'downloads';
$lang['WATERMARK'] = '<br />Designed and developed by <a href="http://retrocompute.net/memberlist.php?mode=viewprofile&u=2">Cryonid</a> and <a href="http://retrocompute.net/memberlist.php?mode=viewprofile&u=48">LiveKiller44</a>, icons and buttons by <a href="https://retrocompute.net/memberlist.php?mode=viewprofile&u=50">Deksor</a><br />Code available on <a href="https://github.com/Cryonid/rcnet-downloadcenter">GitHub</a> under <a href="https://github.com/Cryonid/rcnet-downloadcenter/blob/master/LICENSE">GNU GPLv3 free licence</a>';



//Categories
$lang['IMAGES'] = 'Images';
$lang['UPDATES'] = 'Updates';
$lang['DRIVERS'] = 'Drivers';
$lang['SOFTWARES'] = 'Softwares';
$lang['GAMES'] = 'Games';



//Windows
$lang['BACK'] = 'Back';
$lang['WINNT_TITLE'] = 'Windows NT/2000/XP';
$lang['WIN9X_TITLE'] = 'Windows 95/98/ME';
$lang['MSDOS_TITLE'] = 'MS-DOS/Windows 3.x';



//Right bar
$lang['EDITOR'] = 'Company:';
$lang['DOWNLOAD'] = 'Download';
$lang['LVL0'] = 'Level 0 or unknown';
$lang['LVL1'] = 'Level 1';
$lang['LVL2'] = 'Level 2';
$lang['LVL3'] = 'Level 3';
$lang['LVL4'] = 'Level 4';
$lang['LVL5'] = 'Level 5';
$lang['UNKNOWN_LVL'] = 'dump quality unknown';
$lang['DOWNLOADED'] = 'downloaded';
$lang['TIMES'] = 'times';
$lang['NO_COVER'] = 'No cover has been found. Send your cover to an administrator on the forum through the contact form!';
$lang['FILE_FORMAT'] = 'File format:';
$lang['COMPRESSION'] = 'with compression';
$lang['SHARED_BY'] = 'Shared by:';
$lang['FILE_SOURCE'] = 'File source';
$lang['LANGUAGE'] = 'Language:';
$lang['RELEASE_DATE'] = 'Release date:';
$lang['DATE_FORMAT'] = 'Y-m-d';
$lang['TIME_FORMAT'] = 'h:i A';
$lang['UPLOAD_DATE'] = 'Upload date:';
$lang['DESCRIPTION'] = 'Description:';
$lang['MINIMAL_SYS_REQ'] = 'Minimal system requirements:';
$lang['RECOMMENDED_SYS_REQ'] = 'Recommended system requirements:';
$lang['FILE_SIZE'] = 'File size:';
$lang['UNKNOWN'] = 'Unknown';
$lang['SUPPORTS'] = 'Support(s):';
$lang['SHA1'] = 'SHA1 checksum:';
$lang['MD5'] = 'MD5 checksum:';
$lang['SCREENSHOT'] = 'Screenshot:';
$lang['NO_SCREENSHOT'] = 'No screenshot has been uploaded. Send your screenshots to an administrator!';
$lang['FILE_NOT_FOUND'] = 'The requested file can\'t be found.';
$lang['CHECK_ID_OR_CONTACT'] = 'Please check if the ID or the URL is correct, otherwise please contact an administrator (or select another file in the list).';



//Admin panel
$lang['TXT_FILE_GEN'] = 'Text generator';
$lang['COPY_PASTE'] = 'Copy-paste this text in a .txt file to provide with your download:';
$lang['USERNAME'] = 'Username:';
$lang['PASSWORD'] = 'Password:';
$lang['ERROR_NO_LOGIN'] = 'Error: no username or password.';
$lang['INCORRECT_PASSWORD'] = 'Incorrect password';
$lang['LOGIN_REDIR'] = 'Redirection to the login page...';
$lang['BACK_TO_SITE'] = 'Back to center';
$lang['BACK_TO_ORIGINAL_SITE'] = 'Back to forum';
$lang['ADMIN_HOME'] = 'Panel home';
$lang['ADMIN_ADD'] = 'Add a file';
$lang['ADMIN_MODIFY'] = 'Modify a file';
$lang['ADD_TITLE'] = 'Add a file to the download center';
$lang['CONTENT_NAME'] = 'Content name:';
$lang['COMPANY'] = 'Company:';
$lang['CATEGORY'] = 'Category:';
$lang['OS'] = 'Operating system:';
$lang['WARNING_OS'] = 'WARNING: if the content isn\'t compatible with all operating systems\' group, please mention it in "Minimal system requirements" and "Recommended system requirements" parts';
$lang['FILE_URL'] = 'File URL:';
$lang['FILE_SOURCE'] = 'File(s) source:';
$lang['FILE_FORMAT'] = 'File(s) format:';
$lang['FF_BIN'] = 'BIN/CUE';
$lang['FF_ISO'] = 'ISO';
$lang['FF_IMA'] = 'IMA';
$lang['FF_MDS'] = 'MDS';
$lang['FF_IMG'] = 'IMG';
$lang['FF_OTHER'] = 'Other (txt, pdf, ...)';
$lang['OTHER'] = 'Other';
$lang['NO_COMPRESSION'] = 'without compression';
$lang['DUMP_QUALITY'] = 'Dump quality:';
$lang['LVL'] = 'Level';
$lang['SHARED_BY'] = 'Shared by (nickname/name):';
$lang['INFO_DATE_FORMAT'] = '(if handwritten: AAAA-MM-JJ format, example: January, 8th 1994 with be translated to "1994-01-08")';
$lang['WARNING_LINEBREAK'] = 'Please don\'t do linebreaks!';
$lang['USE_DOT'] = 'WARNING: <strong>Don\'t use comma</strong> and use only <strong>dot</strong> if it\'s a decimal number';
$lang['BYTE'] = 'byte';
$lang['BYTE_S'] = 'byte(s)';
$lang['BYTES'] = 'bytes';
$lang['KB'] = 'KB';
$lang['MB'] = 'MB';
$lang['GB'] = 'GB';
$lang['NB_SUPPORTS'] = 'Supports number (CD/DVD/floppy disks) to use:';
$lang['CD'] = 'CD';
$lang['DVD'] = 'DVD';
$lang['FLOPPY_DISK'] = 'floppy disk';
$lang['FLOPPY_DISKS'] = 'floppy disks';
$lang['FLOPPY_DISK_S'] = 'floppy disk(s)';
$lang['ICON32_URL'] = 'Icon URL (32 x 32 px):';
$lang['ICON16_URL'] = 'Icon URL (16 x 16 px):';
$lang['LIST_LOCAL_ICONS'] = 'See available local icons by clicking here';
$lang['TYPE'] = 'Type';
$lang['FILE_NAME'] = '[file name]';
$lang['USE_LOCAL_ICON'] = 'to use a local icon';
$lang['COVER_URL'] = 'Cover URL:';
$lang['SCREENSHOT_URL'] = 'Screenshot URL:';
$lang['ERROR_SEND'] = 'Error while processing';
$lang['ERROR_ADD'] = 'Some informations are missing, please complete the form entirely';
$lang['SUCCESS_ADD'] = 'File successfully added!';
$lang['SUCCESS_MODIFY'] = 'File successfully modified!';
$lang['SEND_REDIR'] = 'Redirection in 3 seconds to the add page...';
$lang['SELECT_CATEGORY'] = 'Select file\'s category and operating system:';
$lang['SELECT_FILE'] = 'Select file in the list:';
$lang['MODIFY_TITLE'] = 'Modify informations of a download center\'s file';
$lang['KEEP_DEFINED_VALUE'] = 'Keep defined value';
$lang['UNKNOWN_COMPRESSION'] = 'unknown';
?>