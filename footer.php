<footer>
	<a href="<?php echo ($configuration['site_dir'] . 'winnt/'); ?>"><img src="<?php echo ($configuration['site_dir'] . 'images/footer/winnt.gif'); ?>" /><br /><?php echo ($lang['WINNT']); ?></a>
	<a href="<?php echo ($configuration['site_dir'] . 'win9x/'); ?>"><img src="<?php echo ($configuration['site_dir'] . 'images/footer/win9x.gif'); ?>" /><br /><?php echo ($lang['WIN9X']); ?></a>
	<a href="<?php echo ($configuration['site_dir'] . 'msdos/'); ?>"><img src="<?php echo ($configuration['site_dir'] . 'images/footer/msdos.gif'); ?>" /><br /><?php echo ($lang['MSDOS']); ?></a>
	<a href="<?php echo ($configuration['site_dir'] . 'macintosh/'); ?>"><img src="<?php echo ($configuration['site_dir'] . 'images/footer/macintosh.gif'); ?>" /><br /><?php echo ($lang['MACINTOSH']); ?></a>
	<a href="<?php echo ($configuration['site_dir'] . 'linux/'); ?>"><img src="<?php echo ($configuration['site_dir'] . 'images/footer/linux.gif'); ?>" /><br /><?php echo ($lang['LINUX']); ?></a>
	<a href="<?php echo ($configuration['site_dir'] . 'others/'); ?>"><img src="<?php echo ($configuration['site_dir'] . 'images/footer/floppydisk.gif'); ?>" /><br /><?php echo ($lang['OTHERS']); ?></a>

	<p>
		<strong><a href='https://retrocompute.net/viewtopic.php?f=5&t=31'><?php echo ($lang['VERSION'] . ' ' . $configuration['version']); ?></a></strong>
		<br />

		<?php
		$query = $sql->query('SELECT * FROM files');
		$nbfiles = $query->rowCount();

		$nbdownloads = 0;
		while ($data = $query->fetch())
		{
			$nbdownloads = $nbdownloads + $data['nb_downloads'];
		}
        
		$query->closeCursor();

		echo ($nbfiles . ' ' . $lang['NBFILES'] . ' - ' . $nbdownloads . ' ' . $lang['NBDOWNLOADS']);
        echo ($lang['WATERMARK']);
		?>
	</p>
</footer>
