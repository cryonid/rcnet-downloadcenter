<!DOCTYPE html>
<html>
    <?php include_once('../../head.php'); ?>
    <body id="admin">
        <?php
        if (empty($_POST) OR $_POST['quality'] >= 6 OR empty($_POST['name']) OR empty($_POST['company']) OR empty($_POST['system']) OR empty($_POST['download_url']) OR empty($_POST['category']))
        {
            echo ('<p style="color: red;"><b>' . $lang['ERROR_SEND'] . '</b></p>');
            echo ('<p>' . $lang['ERROR_ADD'] . '</p>');
            echo ('<p>' . $lang['SEND_REDIR'] . '</p>');
            header('Refresh: 3; URL=index.php');
        }
        else
        {

        $add = $sql->prepare('UPDATE files SET name=:name, company=:company, category=:category, system=:system, download_url=:download_url, file_format=:file_format, compression=:compression, quality=:quality, version=:version, nickname=:nickname, source=:source, language=:language, release_date=:release_date, upload_date=:upload_date, description=:description, minimal_sys_req=:minimal_sys_req, recommended_sys_req=:recommended_sys_req, file_size=:file_size, size_unit=:size_unit, nb_supports=:nb_supports, support_type=:support_type, sha1=:sha1, md5=:md5, icon16_url=:icon16_url, icon32_url=:icon32_url, cover_url=:cover_url, screenshot_url=:screenshot_url WHERE id = :id');

        $add->execute(array(
            'name' => htmlspecialchars($_POST['name']),
            'company' => htmlspecialchars($_POST['company']),
            'category' => htmlspecialchars($_POST['category']),
            'system' => htmlspecialchars($_POST['system']),
            'download_url' => htmlspecialchars($_POST['download_url']),
            'file_format' => htmlspecialchars($_POST['file_format']),
            'compression' => htmlspecialchars($_POST['compression']),
            'quality' => htmlspecialchars($_POST['quality']),
            'version' => htmlspecialchars($_POST['version']),
            'nickname' => htmlspecialchars($_POST['nickname']),
            'source' => htmlspecialchars($_POST['source']),
            'language' => htmlspecialchars($_POST['language']),
            'release_date' => htmlspecialchars($_POST['release_date']),
            'upload_date' => date('Y-m-d'),
            'description' => htmlspecialchars($_POST['description']),
            'minimal_sys_req' => htmlspecialchars($_POST['minimal_sys_req']),
            'recommended_sys_req' => htmlspecialchars($_POST['recommended_sys_req']),
            'file_size' => htmlspecialchars($_POST['file_size']),
            'size_unit' => htmlspecialchars($_POST['size_unit']),
            'nb_supports' => htmlspecialchars($_POST['nb_supports']),
            'support_type' => htmlspecialchars($_POST['support_type']),
            'sha1' => htmlspecialchars($_POST['sha1']),
            'md5' => htmlspecialchars($_POST['md5']),
            'icon16_url' => htmlspecialchars($_POST['icon16_url']),
            'icon32_url' => htmlspecialchars($_POST['icon32_url']),
            'cover_url' => htmlspecialchars($_POST['cover_url']),
            'screenshot_url' => htmlspecialchars($_POST['screenshot_url']),
            'id' => htmlspecialchars($_POST['id'])
        ));

        echo ('<p style="color: green;"><b>' . $lang['SUCCESS_MODIFY'] . '</b></p>');
        echo ('<p>' . $lang['SEND_REDIR'] . '</p>');
        header('Refresh: 3; URL=index.php');
        }
        ?>
    </body>
</html>