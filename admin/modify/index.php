<!DOCTYPE html>
<html>
	<?php include_once('../../head.php'); ?>

	<body id='admin'>
		<?php include_once('../left_bar.php'); ?>
		<div id='admin_content'>
			<?php
			if (empty($_GET['category']) AND empty($_GET['system']))
			{
				?>

				<form action='' method='get'>
					<h3><?php echo ($lang['SELECT_CATEGORY']); ?></h3>

					<select name='category'>
						<option value='images'><?php echo ($lang['IMAGES']); ?></option>
						<option value='drivers'><?php echo ($lang['DRIVERS']); ?></option>
						<option value='softwares'><?php echo ($lang['SOFTWARES']); ?></option>
						<option value='games'><?php echo ($lang['GAMES']); ?></option>
                        <option value='games'><?php echo ($lang['DRIVERS']); ?></option>
					</select>

					<select name='system'>
						<option value='winnt'><?php echo ($lang['WINNT_TITLE']); ?></option>
						<option value='win9x'><?php echo ($lang['WIN9X_TITLE']); ?></option>
						<option value='msdos'><?php echo ($lang['MSDOS_TITLE']) ?></option>
						<option value='macintosh'><?php echo ($lang['MACINTOSH']); ?></option>
						<option value='linux'><?php echo ($lang['LINUX']); ?></option>
						<option value='others'><?php echo ($lang['OTHERS']); ?></option>
					</select>

					<input type='submit' />
				</form>

				<?php
			}
			else if (empty($_GET['id']))
			{
				{
					?>

					<h3><?php echo ($lang['SELECT_FILE']); ?></h3>

					<?php $query = $sql->query('SELECT * FROM files WHERE system="' . $_GET['system'] . '" AND category="' . $_GET['category'] . '"'); ?>

					<form action='edit.php' method='post'>
						<select name='id'>
							<?php
							while ($data = $query->fetch())
							{
                                if ($data['version'])
                                {
                                    $version = ' (version ' . $data['version'] . ')';
                                }

								echo ('<option value=' . $data['id'] . '>[' . substr(strtoupper($data['language']), 0, 2) . '] ' . $data['name'] . $version . '</option>');
							}
							?>
						</select>

						<input type='hidden' name='system' value=<?php echo ($_GET['system']); ?>>
						<input type='submit' />
					</form>

					<?php
					$query->closeCursor();
				}
			}
			?>
		</div>
	</body>
</html>