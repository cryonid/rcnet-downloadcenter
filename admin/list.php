<?php
function listing($system)
{
	include('../head.php');
	include('language.php');

	$query = $sql->prepare('SELECT * FROM files WHERE system="' . $system . '" AND category=:category');

	switch ($system)
	{
		case 'winnt':
			$system = $lang['WINNT_TITLE'];
		break;
		case 'win9x':
			$system = $lang['WIN9X_TITLE'];
		break;
		case 'msdos':
			$system = $lang['MSDOS_TITLE'];
		break;
		case 'macintosh':
			$system = $lang['MACINTOSH'];
		break;
		case 'linux':
			$system = $lang['LINUX'];
		break;
		case 'others':
			$system = $lang['OTHERS'];
		break;
		default:
			$system = $lang['UNKNOWN'];
		break;
	}
	
	$query->execute(array('category' => 'images'));
	$nb_images = $query->rowCount();
	$query->execute(array('category' => 'drivers'));
	$nb_drivers = $query->rowCount();
	$query->execute(array('category' => 'softwares'));
	$nb_softwares = $query->rowCount();
	$query->execute(array('category' => 'games'));
	$nb_games = $query->rowCount();
	
	$data = array($system, $nb_images, $nb_drivers, $nb_softwares, $nb_games);

	echo ('<h3>' . $system . '</h3><p>' . $nb_images.' '.strtolower($lang['IMAGES']) . ', ' . $nb_drivers.' '.strtolower($lang['DRIVERS']) . ', ' . $nb_softwares.' '.strtolower($lang['SOFTWARES']) . ', ' . $nb_games.' '.strtolower($lang['GAMES']) . '</p>');

	$query->closeCursor();
}

listing('winnt');
listing('win9x');
listing('msdos');
listing('macintosh');
listing('linux');
listing('others');
?>