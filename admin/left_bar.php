<div id='admin_left_bar'>
	<ul>
		<li><a href=<?php echo ($configuration['site_dir']); ?>><?php echo ($lang['BACK_TO_SITE']); ?></a></li>
		<li><a href=<?php echo ($configuration['site_home']) ?>><?php echo ($lang['BACK_TO_ORIGINAL_SITE']); ?></a></li>
		---
		<li><a href=<?php echo ($configuration['site_dir'] . 'admin'); ?>><?php echo ($lang['ADMIN_HOME']) ?></a></li>
		<li><a href=<?php echo ($configuration['site_dir'] . 'admin/add'); ?>><?php echo ($lang['ADMIN_ADD']); ?></a></li>
        <li><a href=<?php echo ($configuration['site_dir'] . 'admin/modify'); ?>><?php echo ($lang['ADMIN_MODIFY']); ?></a></li>
        <li><a href=<?php echo ($configuration['site_dir'] . 'admin/txtfilegenerator.php'); ?>><?php echo ($lang['TXT_FILE_GEN']); ?></a></li>
    </ul>
    <p id='select_lang'>
        <?php echo ($lang['SELECT_LANG']); ?><br />
        <?php
        $language_dir = array_diff(scandir($_SERVER['DOCUMENT_ROOT'] . $configuration['site_dir'] . 'languages', 1), array('.', '..'));

        for ($i = 0; $i < count($language_dir); $i++)
        {
            echo ('<a href=' . $configuration['site_dir'] . '?lang=' . str_replace('.php', '', $language_dir[$i]) . '>[' . strtoupper(str_replace('.php', '', $language_dir[$i])) . ']</a>');
        }
        ?>
    </p>
</div>