<!DOCTYPE html>
<html>
	<?php include_once('../../head.php'); ?>

	<body id='admin'>
		<?php include_once('../left_bar.php'); ?>
		<div id='admin_content'>
			<form action='send.php' method='post'>
                <h3><?php echo ($lang['ADD_TITLE'] . ' ' . $configuration['site_name']); ?></h3>
                
				<?php echo ($lang['CONTENT_NAME']); ?>
				<input type='text' size='48' name='name' /><br />
                <br />
                
				<?php echo ($lang['COMPANY']); ?>
				<input type='text' size='32' name='company' /><br />
                <br />
                
				<?php echo ($lang['CATEGORY']); ?>
				<select name='category'>
					<option value='images'><?php echo ($lang['IMAGES']); ?></option>
                    <option value='updates'><?php echo ($lang['UPDATES']); ?></option>
					<option value='drivers'><?php echo ($lang['DRIVERS']); ?></option>
					<option value='softwares'><?php echo ($lang['SOFTWARES']); ?></option>
					<option value='games'><?php echo ($lang['GAMES']); ?></option>
				</select><br/> 
                <br />
                
				<?php echo ($lang['OS']); ?>
				<select name='system'>
					<option value='winnt'><?php echo ($lang['WINNT_TITLE']); ?></option>
					<option value='win9x'><?php echo ($lang['WIN9X_TITLE']); ?></option>
					<option value='msdos'><?php echo ($lang['MSDOS_TITLE']); ?></option>
					<option value='macintosh'><?php echo ($lang['MACINTOSH']); ?></option>
					<option value='linux'><?php echo ($lang['LINUX']); ?></option>
					<option value='others'><?php echo ($lang['OTHERS']); ?></option>
                </select><br />
                <em><?php echo ($lang['WARNING_OS']); ?></em><br/>
                <br />
                
				<?php echo ($lang['FILE_URL']); ?>
				<input type='text' size='64' name='download_url' /><br />
                <br />
                
                <?php echo ($lang['FILE_SOURCE']); ?>
                <input type='text' size='32' name='source' /><br />
                <br />

				<?php echo ($lang['FILE_FORMAT']); ?>
				<select name='file_format'>
					<option value='bin'><?php echo ($lang['FF_BIN']); ?></option>
					<option value='iso'><?php echo ($lang['FF_ISO']); ?></option>
					<option value='mds'><?php echo ($lang['FF_MDS']); ?></option>
					<option value='ima'><?php echo ($lang['FF_IMA']); ?></option>
                    <option value='img'><?php echo ($lang['FF_IMG']); ?></option>
					<option value='other'><?php echo ($lang['OTHER']); ?></option>
                </select>
				<select name='compression'>
					<option value='0'>(<?php echo ($lang['NO_COMPRESSION']); ?>)</option>
					<option value='1'>(<?php echo ($lang['COMPRESSION']); ?>)</option>
				</select><br />
                <br />
                
				<?php echo ($lang['DUMP_QUALITY']); ?>
				<select name='quality'>
					<optgroup label=<?php echo ($lang['LVL']); ?>>
						<option value='0'><?php echo ($lang['LVL0']); ?></option>
						<option value='1'><?php echo ($lang['LVL1']); ?></option>
						<option value='2'><?php echo ($lang['LVL2']); ?></option>
						<option value='3'><?php echo ($lang['LVL3']); ?></option>
						<option value='4'><?php echo ($lang['LVL4']); ?></option>
						<option value='5'><?php echo ($lang['LVL5']); ?></option>
					</optgroup>
				</select><br />
                <br />
                
				<?php echo ($lang['VERSION']); ?>
				<input type='text' name='version' /><br />
                <br />
                
				<?php echo ($lang['SHARED_BY']); ?>
				<input type='text' name='nickname' /><br />
                <br />
                
				<?php echo ($lang['LANGUAGE']); ?>
                <input type='text' name='language' />
                <em><?php echo ($lang['INFO_LANGUAGE']) ?></em><br />
                <br />
                
                <?php echo ($lang['RELEASE_DATE']); ?>
                <input type='date' name='release_date' /><br />
                <br />
                
				<?php echo ($lang['DESCRIPTION']) ?>*<br />
				<textarea cols='48' rows='4' name='description'></textarea><br />
                <br />
                
				<?php echo ($lang['MINIMAL_SYS_REQ']); ?>*<br />
				<textarea cols='48' rows='4' name='minimal_sys_req'></textarea><br />
                <br />
                
				<?php echo ($lang['RECOMMENDED_SYS_REQ']); ?>*<br />
				<textarea cols='48' rows='4' name='recommended_sys_req'></textarea><br />
                <br />
                
				<em><strong>*<?php echo ($lang['WARNING_LINEBREAK']); ?></strong></em>
                <br /><br />
                
				<?php echo ($lang['FILE_SIZE']); ?>
				<input type='text' size='8' name='file_size' />
				<select name='size_unit'>
					<option value='byte'><?php echo ($lang['BYTE_S']); ?></option>
					<option value='kbyte'><?php echo ($lang['KB']); ?></option>
					<option value='mbyte'><?php echo ($lang['MB']); ?></option>
					<option value='gbyte'><?php echo ($lang['GB']); ?></option>
                </select><br />
                <em><?php echo ($lang['USE_DOT']); ?></em><br />
                <br />
                
				<?php echo ($lang['NB_SUPPORTS']); ?>
				<input type='text' size='8' name='nb_supports' />
				<select name='support_type'>
					<option value='cd'><?php echo ($lang['CD']); ?></option>
					<option value='dvd'><?php echo ($lang['DVD']); ?></option>
					<option value='floppydisk'><?php echo ($lang['FLOPPY_DISK_S']); ?></option>
				</select><br />
                <br />
                
				<?php echo ($lang['SHA1']); ?>
				<input type='text' size='48' name='sha1' /><br />
                <br />
                
				<?php echo ($lang['MD5']); ?>
				<input type='text' size='48' name='md5' /><br />
                <br />
                
				<?php echo ($lang['ICON32_URL']); ?>
				<input type='text' size='64' name='icon32_url' /><br />
                <br />
                
				<?php echo ($lang['ICON16_URL']); ?>
                <input type='text' size='64' name='icon16_url' /><br />
				<em><?php echo ($lang['TYPE'] . ' ' . $configuration['site_dir'] . 'images/icons/' . $lang['FILE_NAME'] . '.gif ' . $lang['USE_LOCAL_ICON'] ); ?></em><br />
    			<em><a href=<?php echo ($configuration['site_dir'] . 'images/icons'); ?>><?php echo ($lang['LIST_LOCAL_ICONS']); ?></a></em><br />
                <br />
                
				<?php echo ($lang['COVER_URL']); ?>
				<input type='text' size='64' name='cover_url' /><br />
                <br />
                
				<?php echo ($lang['SCREENSHOT_URL']); ?>
				<input type='text' size='64' name='screenshot_url' /><br />
				<br />
				
				<input type='submit' />
			</form>
		</div>
	</body>
</html>