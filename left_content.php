<?php
$directory = $_SERVER['REQUEST_URI'];
$system;
$category;

switch ($directory)
{
	case (preg_match('/winnt/', $directory) ? true : false):
		$system = 'winnt';
    break;

	case (preg_match('/win9x/', $directory) ? true : false):
		$system = 'win9x';
    break;

	case (preg_match('/msdos/', $directory) ? true : false):
		$system = 'msdos';
    break;

	case (preg_match('/macintosh/', $directory) ? true : false):
		$system = 'macintosh';
    break;

	case (preg_match('/linux/', $directory) ? true : false):
		$system = 'linux';
    break;

	case (preg_match('/others/', $directory) ? true : false):
		$system = 'others';
    break;
}

switch ($directory)
{
	case (preg_match('/images/', $directory) ? true : false):
		$category = 'images';
    break;

	case (preg_match('/drivers/', $directory) ? true : false):
		$category = 'drivers';
    break;

	case (preg_match('/softwares/', $directory) ? true : false):
		$category = 'softwares';
    break;

	case (preg_match('/games/', $directory) ? true : false):
		$category = 'games';
    break;
}

$query = $sql->query('SELECT * FROM files WHERE system="' . $system . '" AND category="' . $category . '" ORDER BY name ASC');

while ($data = $query->fetch())
{
	if (!preg_match('/id/', $directory))
	{
		$file_url = $directory . '?id=' . $data['id'];
	}
	else
	{
		$file_url = preg_replace('/?id=[0-9]/', '', $directory) . '?id=' . $data['id'];
	}

	if ($data['icon16_url'])
	{
		$icon16 = $data['icon16_url'];
	}
	else
	{
        $icon16 = $configuration['site_dir'] . 'images/window_content/file.gif';
    }

	if (isSet($_GET['id']))
	{
		if ($_GET['id'] == $data['id'])
		{
			if ($system == 'linux')
			{
				$style = 'background: #D28D46; color: white; padding: 2px 0 2px 0;';
			}
    		else
			{
				$style = 'background: #000080; color: white; padding: 2px 0 2px 0;';
			}
		}
        else
        {
            $style = '';
        }
	}

    if ($data['version'])
    {
        $version = ' (version ' . $data['version'] . ')';
    }
    ?>

    <p>
        <a href=<?php echo ($file_url); ?>>
            <img src=<?php echo ($icon16); ?> />
            <span style="<?php echo ($style); ?>">
                <?php echo ('<span style="color: green; vertical-align: initial;">[' . substr(strtoupper($data['language']), 0, 2) . ']</span> ' . $data['name'] . $version); ?>
            </span>
        </a>
    </p>

    <?php
}

$query->closeCursor();
?>