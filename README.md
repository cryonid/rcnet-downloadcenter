# rcnet-downloadcenter
A Windows-skinned download center made to be associated with a website. It was originally made for RETROCOMPUTE.NET, so the download center's theme is focused on old operating systems.

<strong>NOTE : please go here for the new version : [https://gitlab.com/cryonid/retrocompute-net](https://gitlab.com/cryonid/retrocompute-net).</strong>

# How to install the download center ?
<ol>
    <li>Download and extract the download center in your website's desired folder (example: /downloadcenter)</li>
    <li>Import downloadcenter.sql in your SQL database</li>
    <li>Complete the configuration table the following informations :</li>
</ol>
<ul>
    <li>site_name = your site name (example: RETROCOMPUTE.NET)</li>
    <li>site_dir = the directory where the download center has been installed WITH A BEGINNING AND ENDING SLASH! (example: /downloadcenter/)</li>
    <li>site_home = your site home URL (example: https://retrocompute.net)</li>
    <li>version = please don't touch it, it's for me. :)</li>
</ul>

<p>Here is the SQL query to execute if you don't know how to do it: "<strong>UPDATE configuration SET site_name='[YOUR_SITE_NAME]', site_dir='[YOUR_SITE_DIR]', site_home='[YOUR_SITE_HOME]';</strong>" (execute it with the phpMyAdmin's SQL query console or with your machine's SQL console).</p>
<p>(NOTE: I'll probably release an install script to make it more easier, but it's currently not a priority)</p>

<ol start="4">
    <li>Open head.php and complete the $database variable</li>
    <li>You've done with the installation, enjoy!</li>
</ol>

# FAQ

- <strong>Why is there an 'accounts' table but we can access to the admin panel without an account?</strong>
<br />I've planned to do an account system to access to the admin panel, but because I don't have time to do it I didn't made it. You can still easily protect the admin panel with a .htaccess and .htpasswd file.

- <strong>Are you the only one developing the download center?</strong>
<br />As you can see in the watermark, I'm not alone: LiveKiller44 did the code base and some window designs, I improved and lighten his code and did some complex window designs and Deksor found, cut and sometimes made all icons you can see.

- <strong>There is some language files, but the only language available is french. WTF?</strong>
<br />Translations are coming soon!
